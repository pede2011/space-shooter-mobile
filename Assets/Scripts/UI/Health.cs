﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public int health;
    public AudioClip lostHPSFX;
    public AudioClip lowHPLoop;
    public AudioClip enemyPing;

    private Text healthText;
    private AudioSource audioSourcePlayer;
    private AudioSource audioSourceSecondary;
    void Awake()
    {
        if (this.gameObject.GetComponent<AudioSource>() != null)
        {
            audioSourceSecondary = this.gameObject.GetComponent<AudioSource>();
        }
        else
        {
            Debug.Log("HEALTH: COULDNT FIND SECONDARY AUDIO SOURCE");
        }

        GameObject HealthTextGO = GameObject.Find("Health Text");
        if (HealthTextGO != null)
        {
            healthText = HealthTextGO.GetComponent<Text>();
        }
        else
        {
            Debug.Log("HEALTH: COULDNT FIND HEALTH TEXT");
        }

        GameObject playerGO = GameObject.Find("SpaceShip");
        if (playerGO != null)
        {
            audioSourcePlayer = playerGO.GetComponent<AudioSource>();
        }
        else
        {
            Debug.Log("HEALTH: COULDNT FIND PLAYER AUDIOSOURCE");
        }

        UpdateHealth();
    }

    void Update()
    {
        UpdateHealth();       
    }    

    public void LoseHealth(int healthLoss)
    {
        health -= healthLoss;
        UpdateHealth();
        audioSourcePlayer.PlayOneShot(lostHPSFX);
    }

    void UpdateHealth()
    {
        healthText.text = "HP: " + health;

        if (health <= 25 && health != 0)
        {
            audioSourceSecondary.loop = true;
            audioSourceSecondary.clip = lowHPLoop;
            if (audioSourceSecondary.isPlaying == false)
            {
                audioSourceSecondary.Play();
            }
            else
            {
                audioSourceSecondary.Stop();
            }
        }

        if (health <= 0)
        {
            health = 0;
        }
    }

    public bool isDead()
    {
        if (health <= 0)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    public void EnemyHitSFX()
    {
        audioSourcePlayer.PlayOneShot(enemyPing, 0.4f);
    }
}
