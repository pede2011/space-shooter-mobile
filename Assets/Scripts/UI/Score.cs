﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{   
    public int score;
    public static int highscore;

    private Text scoreText;
    private Text highscoreText;

    void Awake()
    {
        score = 0;

        GameObject ScoreTextGO = GameObject.Find("Score Text");
        if (ScoreTextGO != null)
        {
            scoreText = ScoreTextGO.GetComponent<Text>();
        }
        else
        {
            Debug.Log("SCORE: COULDNT FIND SCORE TEXT");
        }

        GameObject HighScoreTextGO = GameObject.Find("HighScore Text");
        if (ScoreTextGO != null)
        {
            highscoreText = HighScoreTextGO.GetComponent<Text>();
        }
        else
        {
            Debug.Log("SCORE: COULDNT FIND HIGHSCORE TEXT");
        }

        UpdateScore();
    }

	void Update ()
    {
        UpdateScore();
	}

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {        
        scoreText.text = "Score: " + score;

        highscore = PlayerPrefs.GetInt("highscore", 0);
        highscoreText.text = "HS: " + highscore;
        if (score > highscore)
        {
            highscore = score;
            PlayerPrefs.SetInt("highscore", highscore);
        }
        
    }
}
