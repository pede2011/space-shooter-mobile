﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class SpaceshipController : MonoBehaviour
{
    public SpawnController spawnController;
    private Health healthController;

    //Shooting related
    public GameObject bullet;
    public Transform shotSpawnLeft;
    public Transform shotSpawnRigth;
    public Transform shotSpawnTop;
    public Transform shotSpawnBot;
    public bool quadShotOn = false;
    public int quadShotNumber;
    public float fireRate;
    private float nextFire;//Tengo que activar el fire rate

    //Movement related
    public float movementForce;
    private float inputDir = 0;

    //Pooling related
    public int pooledBullets;
    List<GameObject> bulletPool;

    //Sounds related
    private AudioSource source;
    public AudioClip audioClip;

    void Start()
    {
        nextFire = fireRate;

        source = this.GetComponent<AudioSource>();

        bulletPool = new List<GameObject>();
        for (int i = 0; i < pooledBullets; i++)
        {
            GameObject obj = (GameObject)Instantiate(bullet);
            obj.SetActive(false);
            bulletPool.Add(obj);
        }

        GameObject spawnControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (spawnControllerObject != null)
        {
            spawnController = spawnControllerObject.GetComponent<SpawnController>();
        }
        else
        {
            Debug.Log("SPACESHIP: COULDNT FIND SCENE CONTROLLER");
        }

        GameObject gameControllerGO = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerGO != null)
        {
            healthController = gameControllerGO.GetComponent<Health>();
        }
        else
        {
            Debug.Log("SPACESHIP: COULDNT FIND GAME CONTROLLER");
        }

        /*if (CrossPlatformInputManager.AxisExists("Horizontal"))
        {
            CrossPlatformInputManager.UnRegisterVirtualAxis("Horizontal");
            CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis("Horizontal");
            CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
        }*/
    }

    void Update()
    {
        //Shooting
        Shoot();

        if (quadShotNumber < 0)
        {
            quadShotOn = false;
            quadShotNumber = 0;
        }

        //Movement
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                inputDir = -1;
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                inputDir = 1;
            }
        }

        else
        {
            inputDir = CrossPlatformInputManager.GetAxis("Horizontal");
        }

        Vector3 movementVector = new Vector3(this.transform.position.x + inputDir * movementForce, this.transform.position.y, this.transform.position.z);

        if (movementVector.x >= 3.8 || movementVector.x <= -3.8)//Hardcoded values because Im lazy, sorry
        {
            movementVector.x = this.transform.position.x;
        }
        this.transform.position = movementVector;
    }

    void Shoot()
    {
        bool isShooting = CrossPlatformInputManager.GetButtonDown("Shoot") || Input.GetKeyDown(KeyCode.Space);

        if (isShooting == true)
        {
            for (int j = 0; j < bulletPool.Count; j++)
            {
                if (bulletPool[j].activeInHierarchy == false)
                {
                    bulletPool[j].transform.position = shotSpawnLeft.position;
                    bulletPool[j].transform.rotation = gameObject.transform.rotation;
                    bulletPool[j].SetActive(true);
                    break;
                }
            }
            for (int i = 0; i < bulletPool.Count; i++)
            {
                if (bulletPool[i].activeInHierarchy == false)
                {
                    bulletPool[i].transform.position = shotSpawnRigth.position;
                    bulletPool[i].transform.rotation = gameObject.transform.rotation;
                    bulletPool[i].SetActive(true);
                    break;
                }
            }
            source.PlayOneShot(audioClip, 0.3f);

            isShooting = false;

        }
    }
}
        /*if (quadShotOn == true)
        {
            bulletPool[j].transform.position = shotSpawnTop.position;
            bulletPool[j].transform.rotation = gameObject.transform.rotation;
            bulletPool[j].SetActive(true);

            j++;

            bulletPool[j].transform.position = shotSpawnBot.position;
            bulletPool[j].transform.rotation = gameObject.transform.rotation;
            bulletPool[j].SetActive(true);

            break;
        }

        else
        {
            break;
        }*/

