﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {

    RaycastHit hit;
    public GameObject crosshair;

	void Awake () 
    {
        crosshair.SetActive(false);
	}
	
	void FixedUpdate () 
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, fwd, out hit, 15))
        {
            if (hit.collider.tag!="Bullet")
            {
                crosshair.SetActive(true);
                crosshair.transform.position = hit.transform.position;
            }
             
        }

        else
        {
            crosshair.SetActive(false);
        }
	}
}
