﻿using UnityEngine;
using System.Collections;

public class LaserSigth : MonoBehaviour{
	public LineRenderer laserRenderer;
	public float laserStartZ;
	public float laserEndZ;
    public RaycastHit hit;

    void FixedUpdate(){
		laserRenderer.SetPosition (1, new Vector3(transform.position.x,transform.position.y, laserStartZ));
		laserRenderer.SetPosition (0, new Vector3(transform.position.x,transform.position.y, laserEndZ));

        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(transform.position, fwd, out hit, 15)){
			if (hit.collider.tag != "Bullet") {
				laserRenderer.SetPosition (0, new Vector3(transform.position.x,transform.position.y, hit.transform.position.z));
			}
        }
    }
}

