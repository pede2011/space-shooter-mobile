﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nuke : MonoBehaviour
{
    private SpawnController spawnController;
    private List<GameObject> enemyList = new List<GameObject>();
    private List<GameObject> bossList = new List<GameObject>();
    private Button nukeButton;

    private bool deployed;
    public AudioClip nukeSFX;
    private AudioSource audioSource;

    void Start ()
    {
        GameObject spawnControllerObject = GameObject.Find("SpawnController");
        if (spawnControllerObject != null)
        {
            spawnController = spawnControllerObject.GetComponent<SpawnController>();
            Debug.Log("GOTTEM");
        }
        else
        {
            Debug.Log("NO SPAWN CONTROLLER FOUND");
        }

        audioSource = this.gameObject.GetComponent<AudioSource>();

        GameObject nukeButtonGO = GameObject.FindGameObjectWithTag("NukeButton");
        if (nukeButtonGO != null)
        {
            nukeButton = nukeButtonGO.GetComponent<Button>(); ;
        }
        else
        {
            Debug.Log("NUKE: COULDNT FIND NUKE BUTTON");
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) && nukeButton.interactable == true)
        {
            deployed = true;
        }

        if (spawnController != null)
        {
            if (spawnController.bossPool != null)
            {
                if (spawnController.bossPool != bossList)
                {
                    bossList = spawnController.bossPool;
                }
            }

            if (spawnController.enemiesPool != null)
            {
                if (spawnController.enemiesPool != enemyList)
                {
                    enemyList = spawnController.enemiesPool;
                }
            }
        }    

        if (deployed==true)
        {
            for (int i = 0; i < bossList.Count; i++)
            {
                bossList[i].SetActive(false);
            }

            for (int j = 0; j < enemyList.Count; j++)
            {
                enemyList[j].SetActive(false);
            }

            audioSource.PlayOneShot(nukeSFX, 0.4f);
            deployed = false;
            nukeButton.interactable = false;
        }
    }

    public void DeployNuke()
    {
        deployed = true;
    }
}
