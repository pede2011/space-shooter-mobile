﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public int comboStartingPoint;
    public float duration;
    private float UseDuration;
    private Combo comboController;
    public AudioClip audioClip;
    private AudioSource audioSource;

	void Start ()
    {
        UseDuration = duration;

        Debug.Log("UNA NAVE VOLO SOBRE MI Y VOLO UN STAR DESTROYER CON SU RASHO LASER");

        GameObject comboControllerGO = GameObject.FindGameObjectWithTag("Combo");
        if (comboControllerGO!=null)
        {
            comboController = comboControllerGO.GetComponent<Combo>();
        }
        else
        {
            Debug.Log("LASER: COULDNT FIND COMBO CONTROLLER");
        }

        GameObject spaceshipControllerGO = GameObject.FindGameObjectWithTag("Player");
        if (spaceshipControllerGO != null)
        {
            audioSource = spaceshipControllerGO.GetComponent<AudioSource>();
        }
        else
        {
            Debug.Log("LASER: COULDNT FIND SPACESHIP CONTROLLER");
        }

        this.gameObject.SetActive(false);
	}
	
	void Update ()
    {        
            audioSource.loop = true;
            audioSource.PlayOneShot(audioClip, 0.3f);

            UseDuration -= Time.deltaTime;
            if (UseDuration <= 0)
            {
                UseDuration = duration;
                audioSource.loop = false;
                audioSource.Stop();
                this.gameObject.SetActive(false);
            }        
	} 
}
