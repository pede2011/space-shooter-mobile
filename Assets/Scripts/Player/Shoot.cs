﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

    public float bulletSpeed;
    //private ColliderManager colliderManager;

    void Start()
    {
        /*GameObject gameControllerGO = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerGO != null)
        {
            colliderManager = gameControllerGO.GetComponent<ColliderManager>();
        }
        else
        {
            Debug.Log("BOUNDBOX: COULDNT FIND GAME CONTROLLER");
        }*/
    }

    void FixedUpdate()//Weird inconsistencies if not
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + bulletSpeed);

        /*if (colliderManager.CheckOutOfBounds(this.transform) == true && this.gameObject.activeInHierarchy == true)
        {
            this.gameObject.SetActive(false);
        }

        if (colliderManager.CheckEnemyAndBullet(this.transform) == true)
        {
            this.gameObject.SetActive(false);
        }*/
    }

    void OnTriggerEnter(Collider other)
    {
        if (this.gameObject.activeInHierarchy == true)
        {
            if (other.gameObject.tag == "OutOfBounds" || other.gameObject.tag == "Enemy" || other.gameObject.tag == "MiniBoss")
            {
                this.gameObject.SetActive(false);
            }
        }
    }     
}
