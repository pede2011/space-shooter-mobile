﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPSCounter : MonoBehaviour {

    float ms;
    float fps;
    float deltaTime = 0.0f;
    Text frames;
    string text;

	void Start ()
    {
        frames = this.GetComponent<Text>();
        ms = 0;
        fps = 0;
        text = "";
	}
	
	void Update () 
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        ms = deltaTime * 1000.0f;
        fps = 1.0f / deltaTime;
        text = string.Format("{1:0]", ms, fps);
        frames.text = text;
	}
}
