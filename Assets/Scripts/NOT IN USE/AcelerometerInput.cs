﻿using UnityEngine;
using System.Collections;

public class AcelerometerInput : MonoBehaviour
{
    //Default position of the device in each axis. 0 is flat, 1 is 90°. It's declared as (0,0,0) just in case.
    private Vector3 defPosition = Vector3.zero;
    // tilt limit between 0 and 1
    float maxTiltDeviceAngleX = 0.6f;
    float maxTiltDeviceAngleY = 0.4f;
    // output curve
    float falloffAngle = 3;
    // input smoothing, higher = faster
    float lerpSpeedX = 250;
    float lerpSpeedY= 250;
    //This are just used internally, do not touch
    private float outputLerpY;
    private float outputLerpX;
    // dead zone between 0 and 1
    float deadzone = 0.1f;
    //Temp variables, just declared here for performance reasons.
    Vector3 newDefPos;

    void Start()
    {
        //Gathering new starting position for the acellerometer, so it doesn't default to "flat" position
        defPosition.y = Input.acceleration.y;
        defPosition.z = Input.acceleration.z;

        //Sets new max tilt angle based on starting phone position
        //maxTiltDeviceAngleY = 1 - defPosition.y;

        Input.compensateSensors = true;//These is more a placebo than anything. If you are using Unity 5, it shouldn't matter. I would leave it there
    }

    void Update()
    {
        // if not using unity 4 or further
        // change Input.acceleration.x to Input.acceleration.y if using landscape

        newDefPos = Vector3.zero;

        //Setting new starting position. Just Y in this case, as X is not an issue
        newDefPos.y = Input.acceleration.y - defPosition.y;
        

        var inputTiltY = Mathf.Clamp(newDefPos.y, -maxTiltDeviceAngleY, maxTiltDeviceAngleY); //Clamp values to max in Y
        var inputTiltX = Mathf.Clamp(Input.acceleration.x, -maxTiltDeviceAngleX, maxTiltDeviceAngleX);//Clamp values to max in X

        //Define new angles based on the values of the clamped inputs, plus the predifined deadzone. Then it's multiplied by the maximum tilt angle, so it doesnt exceed it
        var outputAngleY = Mathf.Pow(Mathf.Clamp(Mathf.Abs(inputTiltY) - deadzone, 0, maxTiltDeviceAngleY) / (maxTiltDeviceAngleY - deadzone), falloffAngle) * maxTiltDeviceAngleY;//Define new angle for Y
        var outputAngleX = Mathf.Pow(Mathf.Clamp(Mathf.Abs(inputTiltX) - deadzone, 0, maxTiltDeviceAngleX) / (maxTiltDeviceAngleX - deadzone), falloffAngle) * maxTiltDeviceAngleX;//Define new angle for X

        //Another clamp, to merge the values of the new angles with the output angles
        var outputDeviceY = Mathf.Clamp(maxTiltDeviceAngleY / inputTiltY, -1, 1) * outputAngleY;
        var outputDeviceX = Mathf.Clamp(maxTiltDeviceAngleX / inputTiltX, -1, 1) * outputAngleX;

        //Lerp defines the speed at which the cube moves, depending on the differences between the received inputs and the output inputs,
        //meaning that faster inputs result in faster objects (Works the same for slow inputs)    
        outputLerpY = Mathf.Lerp(outputLerpY, outputDeviceY, lerpSpeedY * Time.fixedDeltaTime);
        outputLerpX = Mathf.Lerp(outputLerpX, outputDeviceX, lerpSpeedX * Time.fixedDeltaTime);

        // Set new transform for each axis
        var outputY = (outputLerpY / maxTiltDeviceAngleY);
        var outputX = (outputLerpX / maxTiltDeviceAngleX);

        //NOW we move the object. Z is 0 because I don't need it
        transform.Translate(outputX, outputY, 0);
    }
}
