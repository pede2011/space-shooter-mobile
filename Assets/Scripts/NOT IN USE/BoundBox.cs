﻿using UnityEngine;
using System.Collections;

public class BoundBox : MonoBehaviour
{
    private Health healthController;
    public int missedEnemyDMG;

    void Awake()
    {
       
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            if (healthController != null)
            {
                healthController.LoseHealth(missedEnemyDMG);
            }
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "Bullet")
        {
            other.gameObject.SetActive(false);
        }
    }
}
