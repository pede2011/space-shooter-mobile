﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColliderManager : MonoBehaviour
{
    private List<Transform> enemies;
    private List<Transform> bullets;
    private List<Transform> powerups;
    private List<Transform> OutOfBounds;
    private Transform player;

    private List<GameObject> objectsInScene;
    private Scene currentScene;
    private int numberObjectsInScene;

    private float maxZOOB;
    private float minZOOB;

    private Combo comboGO;

    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        if (GameObject.FindGameObjectWithTag("GameController").GetComponent<Combo>() == true)
        {
            comboGO = GameObject.FindGameObjectWithTag("GameController").GetComponent<Combo>();
        }
        else
        {
            Debug.Log("COLLIDERMANAGER: COULDNT FIND GAME CONTROLLER");
        }

        objectsInScene = new List<GameObject>();
        enemies = new List<Transform>();
        bullets = new List<Transform>();
        powerups = new List<Transform>();
        OutOfBounds = new List<Transform>();

        currentScene = SceneManager.GetActiveScene();
        //currentScene.GetRootGameObjects(objectsInScene);
        GetGOInScene();
	}
	
	void Update ()
    {
        GetGOInScene();

        /*CheckEnemyAndBullet();
        CheckEnemyAndOutOfBounds();
        CheckEnemyAndPlayer();
        CheckPlayerAndPU();
        CheckPUAndOutOfBounds();
        CheckBulletAndOutOfBounds();*/
	}

    void GetGOInScene()
    {
        numberObjectsInScene = currentScene.GetRootGameObjects().Length;

        if (objectsInScene.Count < numberObjectsInScene)
        {
            objectsInScene.Clear();
            bullets.Clear();
            enemies.Clear();
            powerups.Clear();
            OutOfBounds.Clear();
            player = null;

            currentScene.GetRootGameObjects(objectsInScene);

            for (int i = 0; i < objectsInScene.Count; i++)
            {
                if (objectsInScene[i].tag == "Player")
                {
                    player = objectsInScene[i].transform;
                }

                if (objectsInScene[i].tag == "Bullet")
                {
                    bullets.Add(objectsInScene[i].transform);
                }

                if (objectsInScene[i].tag == "Enemy")
                {
                    enemies.Add(objectsInScene[i].transform);
                }

                if (objectsInScene[i].tag == "HP" || objectsInScene[i].tag == "Speed")
                {
                    powerups.Add(objectsInScene[i].transform);
                }

                if (objectsInScene[i].tag == "OutOfBounds")
                {
                    OutOfBounds.Add(objectsInScene[i].transform);
                }
            }
        }
    }

    public bool CheckEnemyAndBullet(Transform transformToCheck)
    {
        if (transformToCheck.gameObject.activeInHierarchy == true)
        {

            if (transformToCheck.tag == "Enemy" || transform.tag == "MiniBoss")
            {
                for (int i = 0; i < bullets.Count; i++)
                {
                    if (bullets[i].lossyScale.z > bullets[i].lossyScale.x)//Check longest side
                    {
                        if (IsPointInCircle(bullets[i].position.x, bullets[i].position.z, transformToCheck.position.x, transformToCheck.position.z, bullets[i].lossyScale.z) == true)
                        {
                            comboGO.comboCounter++;
                            return true;
                        }
                    }

                    else
                    {
                        if (IsPointInCircle(bullets[i].position.x, bullets[i].position.z, transformToCheck.position.x, transformToCheck.position.z, bullets[i].lossyScale.x) == true)
                        {
                            comboGO.comboCounter++;
                            return true;
                        }
                    }
                }
            }

            if (transformToCheck.tag == "Bullet")
            {
                for (int i = 0; i < enemies.Count; i++)
                {
                    if (transformToCheck.lossyScale.z > transformToCheck.lossyScale.x)//Check longest side
                    {
                        if (IsPointInCircle(enemies[i].position.x, enemies[i].position.z, transformToCheck.position.x, transformToCheck.position.z, transformToCheck.lossyScale.z) == true)
                        {
                            comboGO.comboCounter++;
                            return true;
                        }
                    }

                    else
                    {
                        if (IsPointInCircle(enemies[i].position.x, enemies[i].position.z, transformToCheck.position.x, transformToCheck.position.z, transformToCheck.lossyScale.x) == true)
                        {
                            comboGO.comboCounter++;
                            return true;
                        }
                    }
                }
            }
        }
   
        return false;
    }

    public bool CheckEnemyAndPlayer(Transform transformToCheck)
    {
        if (transformToCheck.gameObject.activeInHierarchy == true)
        {
            if (player.lossyScale.z > player.lossyScale.x)//Check longest side
            {
                if (IsPointInCircle(player.position.x, player.position.z, transformToCheck.position.x, transformToCheck.position.z, player.lossyScale.z * 10) == true)
                {
                    comboGO.comboCounter = 0;
                    return true;
                }
            }

            else
            {
                if (IsPointInCircle(player.position.x, player.position.z, transformToCheck.position.x, transformToCheck.position.z, player.lossyScale.x * 10) == true)
                {
                    comboGO.comboCounter = 0;
                    return true;
                }
            }
        }
        return false;
    }

    public bool CheckPlayerAndPU(Transform transformToCheck)
    {
        if (transformToCheck.gameObject.activeInHierarchy == true)
        {
            if (player.lossyScale.z > player.lossyScale.x)//Check longest side
            {
                if (IsPointInCircle(player.position.x, player.position.z, transformToCheck.position.x, transformToCheck.position.z, player.lossyScale.z * 10) == true)
                {
                    return true;
                }
            }
            else
            {
                if (IsPointInCircle(player.position.x, player.position.z, transformToCheck.position.x, transformToCheck.position.z, player.lossyScale.x * 10) == true)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool CheckOutOfBounds(Transform transformToCheck)
    {
        if (transformToCheck.gameObject.activeInHierarchy == true)
        {
            for (int j = 0; j < OutOfBounds.Count; j++)
            {
                if (OutOfBounds[j].position.z > maxZOOB)
                {
                    maxZOOB = OutOfBounds[j].position.z;
                }

                if (OutOfBounds[j].position.z < minZOOB)
                {
                    minZOOB = OutOfBounds[j].position.z;
                }

                if (transformToCheck.position.z >= maxZOOB || transformToCheck.position.z <= minZOOB)
                {
                    return true;
                }
            }
        }
        return false;
    }     

    private bool IsPointInCircle(float xa, float ya, float xc, float yc, float r)//Thanks Google!
    {
        return ((xa - xc) * (xa - xc) + (ya - yc) * (ya - yc)) < r * r;
    }
}
