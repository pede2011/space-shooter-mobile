﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Combo : MonoBehaviour
{
    public int comboCounter;
    //private ColliderManager colliderManager;
    private Text comboText;
    private Laser laserComponent;

	void Awake ()
    {
        GameObject comboGO = GameObject.FindGameObjectWithTag("Combo");

        if (comboGO.GetComponent<Text>() != null)
        {
            comboText = comboGO.gameObject.GetComponent<Text>();
        }
        else
        {
            Debug.Log("COMBO: COULDNT FIND UI TEXT");
        }

        GameObject laserGO = GameObject.FindGameObjectWithTag("Laser");
        if (laserGO != null)
        {
            laserComponent = laserGO.gameObject.GetComponent<Laser>();
        }
        else
        {
            Debug.Log("COMBO: COULDNT FIND RASHO LASER");
        }

        /* if (this.gameObject.GetComponent<ColliderManager>() != null)
         {
             colliderManager = this.gameObject.GetComponent<ColliderManager>();
         }
         else
         {
             Debug.Log("COMBO: COULDNT FIND COLLIDER MANAGER");
         }*/
    }
	
	void Update ()
    {
        comboText.text = "Combo: x" + comboCounter;

        if ((comboCounter % laserComponent.comboStartingPoint) == 0 && comboCounter > 0)
        {
            if (laserComponent.gameObject.activeInHierarchy == false)
            {
                laserComponent.gameObject.SetActive(true);
            }
        }
    }   
}
