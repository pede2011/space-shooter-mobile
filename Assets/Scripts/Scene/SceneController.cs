﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class SceneController : MonoBehaviour
{
    public GameObject loadingScreen;
    public GameObject gameOverScreen;
    public GameObject UI;
    public float gameOverDelay;
    private float timer = 0.0f;

    private GameObject player;
    private Health healthController;
    private AudioSource audioSourceMain;
    private AudioSource audioSourceSecondary;
    private InterstitialAd interstitial;

    //ADS
    private BannerView bannerView;

    void Start()
    {
#if UNITY_ANDROID
        Application.targetFrameRate = 60;
#endif
        //ADS
        string appId = "ca-app-pub-3940256099942544~3347511713";       
 
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

        //Request Ad
        RequestInterstitial();

        //GAME
        audioSourceMain = this.gameObject.GetComponent<AudioSource>();

        if (SceneManager.GetActiveScene().name == "Main")
        {
            GameObject GameControllerGO = GameObject.FindGameObjectWithTag("GameController");
            if (GameControllerGO != null)
            {
                healthController = GameControllerGO.GetComponent<Health>();
                audioSourceSecondary = GameControllerGO.GetComponent<AudioSource>();
            }
            else
            {
                Debug.Log("SCENECONTROLLER: COULDNT FIND GAME CONTROLLER");
            }
        }       

        player = GameObject.Find("SpaceShip");
        if (player = null)
        {
            Debug.Log("SCENECONTROLLER: CANT FIND PLAYER");
        }

        if (loadingScreen != null)
        {
            loadingScreen.SetActive(false);
        }

        if (gameOverScreen != null)
        {
            gameOverScreen.SetActive(false);            
        }
    }

    void Update()
    {
        if (healthController && healthController.isDead() == true)
        {
            GameOver();
        }              
    }   

    public void QuitGame()
    {
        Application.Quit();
    }

    public void BackMenu()
    {
        if (loadingScreen != null)
        {
            loadingScreen.SetActive(true);
        }

        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }

        SceneManager.LoadScene("Menu");
    }

    public void LoadGame()
    {
        if (loadingScreen != null)
        {
            loadingScreen.SetActive(true);
        }
        SceneManager.LoadScene("Main");
    }

    void GameOver()
    {
        audioSourceMain.Stop();
        audioSourceSecondary.Stop();
        if (UI != null)
        {
            UI.SetActive(false);
        }
        if (player != null)
        {
            player.SetActive(false);
        }      

        gameOverScreen.SetActive(true);

        for (int i = 0; i <= gameOverDelay; i++)
        {
            timer += Time.deltaTime;
        }

        if (timer >= gameOverDelay)
        {
            BackMenu();
        }       
    }

    private void RequestInterstitial()
    {
        string adUnitId = "ca-app-pub-3940256099942544/1033173712";   
  
        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }
}
