﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpawnController : MonoBehaviour
{
    //Enemy spawn related
    public GameObject enemy;
    public float timeDelay;
    //public GameObject enemy2;
    public float spawnX1;
    public float spawnX2;
    public float spawnZ;
    public float enemies1OnScreen;
    //public float enemies2OnScreen;

    private Health healthController;

    //Pooling related
    public List<GameObject> enemiesPool;    
    //List<GameObject> enemiesPool2;

    //GUI related
    private Score scoreController;

    //PowerUps related
    List<GameObject> puPool;
    public int pooledPU;
    public int scoreForPU;
    private int ogScoreForPU;
    public GameObject PUHealth;
    public GameObject PUSpeed;

    //Boss related
    public List<GameObject> bossPool;
    public int scoreForBoss;
    private int ogScoreForBoss;
    public int cantBoss;
    public GameObject miniBoss;

    public int scoreThreshold;

    void Awake()
    {
        GameObject gameControllerGO = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerGO != null)
        {
            healthController = gameControllerGO.GetComponent<Health>();
            scoreController = gameControllerGO.GetComponent<Score>();
        }
        else
        {
            Debug.Log("SPAWN: COULDNT FIND GAME CONTROLLER");
        }

        ogScoreForBoss = scoreForBoss;
        ogScoreForPU = scoreForPU;

        //Pool enemies
        enemiesPool = new List<GameObject>();
        for (int i = 0; i < enemies1OnScreen; i++)
        {
            GameObject obj = (GameObject)Instantiate(enemy);
            obj.SetActive(false);
            enemiesPool.Add(obj);
        }

       /* //Pool enemies 2
        enemiesPool2 = new List<GameObject>();
        for (int i = 0; i < enemies2OnScreen; i++)
        {
            GameObject obj = (GameObject)Instantiate(enemy2);
            obj.SetActive(false);
            enemiesPool2.Add(obj);
        }*/

        //Pool PU
        puPool = new List<GameObject>();
        for (int i = 0; i < pooledPU; i++)
        {
            GameObject obj = (GameObject)Instantiate(PUHealth);
            obj.SetActive(false);
            puPool.Add(obj);
            GameObject obj2 = (GameObject)Instantiate(PUSpeed);
            obj2.SetActive(false);
            puPool.Add(obj2);
        }

        //Pool boss. Es uno solo pero asi ya lo tengo precargado y no tengo que generar codigo nuevo. Aparte, si dps decido tener mas, así es + flexible
        bossPool = new List<GameObject>();
        for (int i = 0; i < cantBoss; i++)
        {
            GameObject obj = (GameObject)Instantiate(miniBoss);
            obj.SetActive(false);
            bossPool.Add(obj);
        }

    }

    void Update()
    {
       // UpdateHighScore();
        SpawnEnemies();
        //SpawnEnemies2();
        SpawnPU();
        SpawnMiniBoss();
        MoreEnemies();
    }   

    void SpawnEnemies()
    {
        if (healthController.health > 0)
        {
            for (int i = 0; i < enemies1OnScreen; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(spawnX1, spawnX2), 0, spawnZ);//NO Y NEEDED, 2D
                Quaternion spawnRotation = Quaternion.identity;
                for (int j = 0; j < enemiesPool.Count; j++)
                {
                    if (!enemiesPool[j].activeInHierarchy)
                    {
                        float random = Random.Range(0.0f, timeDelay + 1.0f);
                        random -= Time.deltaTime;
                        if (random <= 0)
                        {
                            enemiesPool[j].transform.position = spawnPosition;
                            enemiesPool[j].transform.rotation = spawnRotation;
                            enemiesPool[j].SetActive(true);
                            break;
                        }                       
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < enemiesPool.Count; i++)
            {
                if (enemiesPool[i].activeInHierarchy)
                {
                    enemiesPool[i].SetActive(false);
                    //break;
                }
            }
        }
    }

    /*void SpawnEnemies2()
    {
        if (healthController.health > 0)
        {
            for (int i = 0; i < enemies2OnScreen; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(spawnX1, spawnX2), 0, spawnZ);//NO Y NEEDED, 2D
                Quaternion spawnRotation = Quaternion.identity;
                for (int j = 0; j < enemiesPool2.Count; j++)
                {
                    if (!enemiesPool2[j].activeInHierarchy)
                    {
                        enemiesPool2[j].transform.position = spawnPosition;
                        enemiesPool2[j].transform.rotation = spawnRotation;
                        enemiesPool2[j].SetActive(true);
                        break;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < enemiesPool2.Count; i++)
            {
                if (enemiesPool2[i].activeInHierarchy)
                {
                    enemiesPool2[i].SetActive(false);
                    //break;
                }
            }
        }
    }*/

    void SpawnMiniBoss()
    {
        if (scoreController.score >= scoreForBoss)
        {
            for (int i = 0; i < cantBoss; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(spawnX1, spawnX2), 0, spawnZ);         
                for (int j = 0; j < bossPool.Count; j++)
                {
                    if (!bossPool[j].activeInHierarchy)
                    {
                        bossPool[j].transform.position = spawnPosition;
                        bossPool[j].transform.rotation = miniBoss.transform.rotation;
                        bossPool[j].SetActive(true);
                        break;
                    }
                }
            }
            scoreForBoss += ogScoreForBoss;
        }
    }

    void SpawnPU()
    {
        if (scoreController.score >= scoreForPU)
        {      
            for (int i = 0; i < pooledPU; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(spawnX1, spawnX2), 0, spawnZ);//Coordenadas Y hardcodeadas porque era necesario
                //Quaternion spawnRotation = Quaternion.identity;
                for (int j = 0; j < puPool.Count; j++)
                {
                    if (!puPool[j].activeInHierarchy)
                    {
                        puPool[j].transform.position = spawnPosition;
                        puPool[j].transform.rotation = PUHealth.transform.rotation;//Could have used either one really
                        puPool[j].SetActive(true);
                        break;
                    }
                }
            }            
            scoreForPU += ogScoreForPU;
        }
    }

    void MoreEnemies()
    {
        if ((scoreController.score % scoreThreshold) == 0 && scoreController.score > 0)
        {
            enemies1OnScreen++;
            if (enemies1OnScreen > enemiesPool.Count)
            {
                for (int i = 0; i < enemies1OnScreen-enemiesPool.Count; i++)
                {
                    GameObject obj = (GameObject)Instantiate(enemy);
                    obj.SetActive(false);
                    enemiesPool.Add(obj);
                }                
            }
        }
    }

    /*public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    /*void UpdateScore()
    {
        scoreText.GetComponent<Text>().text = "Score: " + score;
    }

    public void LoseHealth(int healthLoss)
    {
        health -= healthLoss;
        UpdateHealth();
        this.GetComponent<AudioSource>().PlayOneShot(lostHPAudio);
    }

    void UpdateHealth()
    {
        healthText.GetComponent<Text>().text = "HP: " + health;

        if (health<=25 && health!=0)
        {
            this.GetComponent<AudioSource>().loop = true;
            this.GetComponent<AudioSource>().clip = lowHPAudio;
            this.GetComponent<AudioSource>().Play();            
        }

        if (health==0)
        {
            this.GetComponent<AudioSource>().Stop();
            spaceship.gameObject.SetActive(false);
            SceneManager.LoadScene("Main");
        }
    }

    void UpdateHighScore()
    {
        highscore = PlayerPrefs.GetInt("highscore", 0);
        highScoreText.GetComponent<Text>().text = "HS: " + highscore;
        if (score > highscore)
        {
            highscore = score;
            PlayerPrefs.SetInt("highscore", highscore);
        }
    }*/
}