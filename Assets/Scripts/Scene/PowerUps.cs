﻿using UnityEngine;
using System.Collections;

public class PowerUps : MonoBehaviour {

    public float puSpeed = 0;

    //Score related
    public int hpValue;
    public float speedValue;
    public float speedLimit;
    public float rotation;

    private Health healthController;
    //private ColliderManager colliderManager;
    private SpaceshipController spaceshipController;

    private int badChance;

    void Start()
    {
        GameObject gameControllerGO = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerGO != null)
        {
            healthController = gameControllerGO.GetComponent<Health>();
            //colliderManager = gameControllerGO.GetComponent<ColliderManager>();
        }
        else
        {
            Debug.Log("POWER UP: COULDNT FIND GAME CONTROLLER");
        }

        GameObject playerGO = GameObject.Find("SpaceShip");
        if (playerGO != null)
        {
            spaceshipController = playerGO.GetComponent<SpaceshipController>();
        }
        else
        {
            Debug.Log("POWER UP: COULDNT FIND PLAYER");
        }

       /* badChance = Random.Range(0, 4);

        if (badChance == 1)
        {
            if (this.gameObject.tag=="Speed")
            {
                speedValue = -speedValue;
                this.GetComponent<Renderer>().material.color = Color.black;
            }

            if (this.gameObject.tag == "HP")
            {
                hpValue = -hpValue;
                this.GetComponent<Renderer>().material.color = Color.black;               
            }

        }*/
    }

    void Update()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + puSpeed);
       /* if (colliderManager.CheckPlayerAndPU(this.transform) == true && this.gameObject.activeInHierarchy == true)
        {
            if (this.gameObject.tag == "HP")
            {
                healthController.LoseHealth(-hpValue);//Estoy haciendo doble negativo para que sume la vida porque me da fiaca hacer otra funcion
                this.gameObject.SetActive(false);
            }
            if (this.gameObject.tag == "Speed")
            {
                if (spaceshipController.movementForce <= speedLimit)
                {
                    spaceshipController.movementForce += speedValue;
                    if (spaceshipController.movementForce >= speedLimit)
                    {
                        spaceshipController.movementForce = speedLimit;
                    }
                    this.gameObject.SetActive(false);
                }
            }
        }

        if (colliderManager.CheckOutOfBounds(this.transform)== true && this.gameObject.activeInHierarchy == true)
        {
            this.gameObject.SetActive(false);
        }*/
    }

    void OnTriggerEnter(Collider other)
    {
        if (this.gameObject.activeInHierarchy == true && other.gameObject.tag == "Player")
        {
            if (this.gameObject.tag == "HP")
            {
                healthController.LoseHealth(-hpValue);//Estoy haciendo doble negativo para que sume la vida porque me da fiaca hacer otra funcion
                this.gameObject.SetActive(false);
            }
            if (this.gameObject.tag == "Speed")
            {
                if (spaceshipController.movementForce <= speedLimit)
                {
                    spaceshipController.movementForce += speedValue;
                    if (spaceshipController.movementForce >= speedLimit)
                    {
                        spaceshipController.movementForce = speedLimit;
                    }
                    this.gameObject.SetActive(false);
                }
            }
        }

        if (this.gameObject.activeInHierarchy == true && other.gameObject.tag == "OutOfBounds")
        {
            this.gameObject.SetActive(false);
        }
    }
}
