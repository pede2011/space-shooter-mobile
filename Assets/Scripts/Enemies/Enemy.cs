﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public float enemySpeed=0;
    public float speedMultiplier;

    //Score related
    public int scoreValue;
    public int dmgValue;
    public int missValue;
    private Score scoreController;
    private Combo comboController;    

    //Health related
    private Health healthController;
    private GameObject player;

    //Movement related
    private int random;
    private int numberOfPatterns = 3;

    //Collision related
    //private ColliderManager colliderManager;

    void Start()
    {
        player = GameObject.Find("SpaceShip");
     
        GameObject GameControllerGO = GameObject.FindGameObjectWithTag("GameController");
        if (GameControllerGO != null)
        {
            scoreController = GameControllerGO.GetComponent<Score>();
            healthController = GameControllerGO.GetComponent<Health>();
            comboController = GameControllerGO.GetComponent<Combo>();
            //colliderManager = GameControllerGO.GetComponent<ColliderManager>();

        }
        else
        {
            Debug.Log("ENEMY: NO GAME CONTROLLER FOUND");
        }

        random = Random.Range(1, numberOfPatterns + 1);

        switch (random)
        {
            case 1: this.GetComponent<Renderer>().material.color = Color.green;
                break;
            case 2:  this.GetComponent<Renderer>().material.color = Color.cyan;
                break;
            case 3: this.GetComponent<Renderer>().material.color = Color.magenta;
                break;
        }
    }

    void Update()
    {
        if (random == 1)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + enemySpeed);

        }

        if (random == 2)
        {
            if ((this.transform.position.x + Mathf.Sin(Time.time) * Time.deltaTime) >= 3.8 || (this.transform.position.x + Mathf.Sin(Time.time) * Time.deltaTime) <= -3.8)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + enemySpeed);
            }
            else
            {
                this.transform.position = new Vector3(this.transform.position.x + Mathf.Sin(Time.time) * Time.deltaTime, this.transform.position.y, this.transform.position.z + enemySpeed);
            }           

        }

        if (random == 3)
        {
            if ((this.transform.position.x + Mathf.Cos(Time.time) * Time.deltaTime) >= 3.8 || (this.transform.position.x + Mathf.Cos(Time.time) * Time.deltaTime) <= -3.8)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + enemySpeed * speedMultiplier);
            }
            else
            {
                this.transform.position = new Vector3(this.transform.position.x + Mathf.Cos(Time.time) * Time.deltaTime, this.transform.position.y, this.transform.position.z + enemySpeed * speedMultiplier);
            }

        }
        /*switch (random)
        {
            case '1':
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + enemySpeed);
                break;
            case '2':
                this.transform.position = new Vector3(this.transform.position.x + Mathf.Sin(Time.time) * Time.deltaTime, this.transform.position.y, this.transform.position.z + enemySpeed);
                break;
            case '3':
                this.transform.position = new Vector3(this.transform.position.x + Mathf.Cos(Time.time) * Time.deltaTime, this.transform.position.y, this.transform.position.z + enemySpeed);
                break;
        }*/


        /*if (colliderManager.CheckEnemyAndBullet(this.transform)==true && this.gameObject.activeInHierarchy == true)
        {
            scoreController.AddScore(scoreValue);
            healthController.EnemyHitSFX();
            this.gameObject.SetActive(false);
        }

        if (colliderManager.CheckEnemyAndPlayer(this.transform)== true && this.gameObject.activeInHierarchy == true)
        {
            healthController.LoseHealth(dmgValue);
            this.gameObject.SetActive(false);
        }

        if (colliderManager.CheckOutOfBounds(this.transform) == true && this.gameObject.activeInHierarchy == true)
        {
            healthController.LoseHealth(missValue);
            this.gameObject.SetActive(false);
        }*/
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet" && this.gameObject.activeInHierarchy == true)
        {
            scoreController.AddScore(scoreValue);
            healthController.EnemyHitSFX();
            comboController.comboCounter++;
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "Player" && this.gameObject.activeInHierarchy == true)
        {
            healthController.LoseHealth(dmgValue);
            comboController.comboCounter = 0;
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "OutOfBounds" && this.gameObject.activeInHierarchy == true)
        {
            healthController.LoseHealth(missValue);
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "Laser" && this.gameObject.activeInHierarchy == true)
        {
            scoreController.AddScore(scoreValue);
            healthController.EnemyHitSFX();
            this.gameObject.SetActive(false);
        }
    }
}
