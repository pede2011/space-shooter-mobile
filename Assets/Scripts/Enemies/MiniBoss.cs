﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MiniBoss : MonoBehaviour {

    public float enemySpeed = 0;

    //Score related
    public int scoreValue;
    public int dmgValue;
    public int missValue;
    public int bossHP;
    public int HPReward;
    public int dmgTaken;

    private int ogBossHP;
    private Health healthController;
    private Score scoreController;
    private Button nukeButton;
    //private ColliderManager colliderManager;

    void Start()
    {
        ogBossHP = bossHP;
                
        GameObject gameControllerGO = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerGO != null)
        {
            healthController = gameControllerGO.GetComponent<Health>();
            scoreController = gameControllerGO.GetComponent<Score>();
            //colliderManager = gameControllerGO.GetComponent<ColliderManager>();
        }
        else
        {
            Debug.Log("MINIBOSS: NO GAME CONTROLLER FOUND");
        }

        GameObject nukeButtonGO = GameObject.FindGameObjectWithTag("NukeButton");
        if (nukeButtonGO != null)
        {
            nukeButton = nukeButtonGO.GetComponent<Button>(); ;
        }
        else
        {
            Debug.Log("MINIBOSS: COULDNT FIND NUKE BUTTON");
        }
    }

    void Update()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + enemySpeed);

        /*if (colliderManager.CheckEnemyAndBullet(this.transform) == true && this.gameObject.activeInHierarchy == true)
        {
            bossHP -= dmgTaken;
            healthController.EnemyHitSFX();

            if (bossHP <= 0)
            {
                scoreController.AddScore(scoreValue);
                healthController.LoseHealth(-HPReward);
                spawnController.nukesEarned++;
                this.gameObject.SetActive(false);
                bossHP = ogBossHP;
            }           
        }

        if (colliderManager.CheckEnemyAndPlayer(this.transform) == true && this.gameObject.activeInHierarchy == true)
        {
            healthController.LoseHealth(dmgValue);
            this.gameObject.SetActive(false);
        }

        if (colliderManager.CheckOutOfBounds(this.transform) == true && this.gameObject.activeInHierarchy == true)
        {
            healthController.LoseHealth(missValue);
            this.gameObject.SetActive(false);
        }*/
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet" == true && this.gameObject.activeInHierarchy == true)
        {
            bossHP -= dmgTaken;
            healthController.EnemyHitSFX();

            if (bossHP <= 0)
            {
                scoreController.AddScore(scoreValue);
                healthController.LoseHealth(-HPReward);
                nukeButton.interactable = true;
                bossHP = ogBossHP;
                this.gameObject.SetActive(false);
            }
        }

        if (other.gameObject.tag == "Player" == true && this.gameObject.activeInHierarchy == true)
        {
            healthController.LoseHealth(dmgValue);
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "OutOfBounds" == true && this.gameObject.activeInHierarchy == true)
        {
            healthController.LoseHealth(missValue);
            this.gameObject.SetActive(false);
        }
    }
}